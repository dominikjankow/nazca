Simulink.importExternalCTypes(which('kortex_wrapper_data.h'));
gen3Kinova = kortex();
gen3Kinova.ip_address = '82.145.75.250';%zmieniono na ten z linku%
gen3Kinova.user = 'admin';
gen3Kinova.password = 'admin';


isOk = gen3Kinova.CreateRobotApisWrapper();
if isOk
   disp('You are connected to the robot!'); 
else
   error('Failed to establish a valid connection!'); 
end
[isOk,baseFb, actuatorFb, interconnectFb] = gen3Kinova.SendRefreshFeedback();
if isOk
%     disp('Base feedback');
%     disp(baseFb);
%     disp('Actuator feedback');
%     disp(actuatorFb);
%     disp('Gripper feedback');
%     disp(interconnectFb);
else
    error('Failed to acquire sensor data.'); 
end









toolCommand = int32(2);    % Velocity control mode
toolDuration = 0;
toolCmd = -1; % Close the gripper with full speed
isOk = gen3Kinova.SendToolCommand(toolCommand, toolDuration, toolCmd);
if isOk
    disp('Command sent to the gripper. Wait for the gripper to close.')
else
    error('Command Error.');
end


isOk = gen3Kinova.DestroyRobotApisWrapper();% rozłączenie kontroli z robotem, problemy bo zanim skończy ruch rozłącza%
clear;